# OneJournal

OneJournal provides a unified view for authoring journals.

## Features

- All journals open in a single OneJournal window
- Custom directory within OneJournal
- Maximized mode
- History

![demonstration](https://cdn.discordapp.com/attachments/648215359895240715/712319778462892192/one-journal-v1-maximized.gif)

## Installation

```
https://gitlab.com/fvtt-modules-lab/one-journal/-/jobs/artifacts/master/raw/module.json?job=build-module
```

### Unstable branch

If you want to live on the edge, you can use the latest version from the
`develop` branch, but there may be unfinished features and rough edges.

```
https://gitlab.com/fvtt-modules-lab/one-journal/-/jobs/artifacts/develop/raw/module.json?job=build-unstable
```

## Changelog

See [CHANGELOG.md](/CHANGELOG.md)

## Compatibility

- Please report any incompatible modules or systems by creating an issue, or
  contact me on discord.
- Compatible with D&D5e Dark Mode
