// Extra types not covered by foundry-vtt-types

interface Window {
  oneJournal: any;
}

declare class PermissionViewer {
  static directoryRendered: any;
}

interface LenientGlobalVariableTypes {
  game: never;
}
