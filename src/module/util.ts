export const i18n = (name: string): string =>
  game.i18n.localize(`ONEJOURNAL.${name}`);
