import { JournalShell } from "./module/journalShell.js";
import { getSetting, settings, registerSettings } from "./module/settings.js";
import { i18n } from "./module/util.js";

class OneJournal {
  shell: JournalShell;
  openButton: JQuery<HTMLElement>;

  // Listen to changes of mode swapping to support detached windows
  hookSwapMode() {
    //@ts-ignore
    const oldOnSwapMode = JournalSheet.prototype._onSwapMode;
    //@ts-ignore
    JournalSheet.prototype._onSwapMode = function (event, mode) {
      if (
        (mode === "image" && this._sheetMode === "text") ||
        (mode === "text" && this._sheetMode === "image")
      ) {
        window.oneJournal?.shell.swappingJournals.add(this.document.uuid);
      }

      return oldOnSwapMode.call(this, event, mode);
    };
  }

  onRenderJournalShell(app: JournalSheet, html: JQuery<HTMLElement>): void {
    this._onJournalAdded(app);

    if (getSetting(settings.DBL_CLICK_EDIT) === true) {
      html.find(".editor").dblclick((evt) => {
        if (evt.target.closest(".editor-content")) {
          $(app.element).find(".editor-edit").click();
        }
      });
    }
  }

  init() {
    this.shell = new JournalShell();
    this.hookSwapMode();

    Hooks.on("closeJournalSheet", (app: JournalSheet) => {
      this._onJournalRemoved(app);
    });
  }

  _onJournalAdded(sheet: JournalSheet) {
    if (getSetting(settings.USE_ONE_JOURNAL) === false) {
      return;
    }
    this.shell.open(sheet);
  }

  _onJournalRemoved(app: JournalSheet) {
    this.shell.detach(app);
  }

  toggleOpenButton(show: boolean) {
    if (show) {
      window.oneJournal.openButton.css("display", "block");
    } else {
      window.oneJournal.openButton.css("display", "none");
    }
  }
  userPermitted(): boolean {
    return !(getSetting(settings.GM_ONLY) === true && !game.user.isGM);
  }
}

Hooks.on(
  "renderJournalSheet",
  (app: JournalSheet, html: JQuery<HTMLElement>) => {
    if (!app.popOut) {
      // GM Screen Renders journalSheets without popOut
      return;
    }

    window.oneJournal?.onRenderJournalShell(app, html);
  }
);

Hooks.once("init", async function () {
  registerSettings({
    [settings.SIDEBAR_MODE]: (val) => {
      window.oneJournal.shell.changeSidebarMode(val);
    },
    [settings.OPEN_BUTTON_IN_DIRECTORY]: (val) => {
      window.oneJournal.toggleOpenButton(val);
    },
    [settings.SIDEBAR_WIDTH]: (val) => {
      window.oneJournal?.shell?.setSidebarWidth(val);
    },
    [settings.SIDEBAR_COMPACT]: (val) => {
      window.oneJournal?.shell?.directory?.setSidebarCompact(val);
    },
    [settings.FOLDER_SELECTOR]: (val) => {
      if (val) {
        window.oneJournal?.shell?.element?.addClass("show-folder-select");
      } else {
        window.oneJournal?.shell?.element?.removeClass("show-folder-select");
      }
    },
  });

  CONFIG.TinyMCE.css?.push("/modules/one-journal/editor.css");
  if (typeof CONFIG.TinyMCE.content_css === "object") {
    CONFIG.TinyMCE.content_css?.push("/modules/one-journal/editor.css");
  }

  window.oneJournal = window.oneJournal || new OneJournal();
  // Button in sidebar directory
  Hooks.on(
    "renderJournalDirectory",
    (app: JournalSheet, html: JQuery<HTMLElement>) => {
      if (
        !window.oneJournal.userPermitted() ||
        html.closest("#OneJournalDirectory").length != 0
      ) {
        return;
      }

      window.oneJournal.openButton = $(
        `<button class="one-journal-open">${i18n("OpenButton")}</button>`
      );
      window.oneJournal.openButton.click(() => {
        window.oneJournal.shell.render(true);
      });
      html.find(".directory-footer").append(window.oneJournal.openButton);

      window.oneJournal.toggleOpenButton(
        getSetting(settings.OPEN_BUTTON_IN_DIRECTORY)
      );
    }
  );

  // Patch links for opening detached
  {
    // Entity links in enriched html
    //@ts-ignore
    const oldOnClickContentLink = TextEditor._onClickContentLink;
    const onClickContentLink = async function (event: JQuery.ClickEvent) {
      console.log("overridden event...", event);
      const a = event.currentTarget as HTMLElement;

      if (!event.shiftKey) {
        oldOnClickContentLink(event);
        return;
      }

      let uuid = "";
      if (
        a.dataset.pack &&
        game.packs.get(a.dataset.pack)?.documentName === "JournalEntry"
      ) {
        uuid = `Compendium.${a.dataset.pack}.${a.dataset.id}`;
      } else if (a.dataset.entity === "JournalEntry") {
        uuid = `${a.dataset.entity}.${a.dataset.id}`;
      } else {
        oldOnClickContentLink(event);
        return;
      }
      event.preventDefault();
      window.oneJournal.shell.openDetached(uuid);
    };
    //@ts-ignore
    TextEditor._onClickContentLink = onClickContentLink;

    // Journal sidebar link handler
    //@ts-ignore
    const oldOnClickEntityName = JournalDirectory._onClickEntityName;
    const onClickEntityName = function (event) {
      event.preventDefault();
      const element = event.currentTarget;
      const entityId = element.parentElement.dataset.entityId;
      const entity = game.journal.get(entityId);
      if (event.shiftKey) {
        window.oneJournal.shell.openDetached(entity.uuid);
        return;
      }
      const sheet = entity.sheet;
      // @ts-ignore
      if (sheet._minimized) return sheet.maximize();
      else return sheet.render(true);
    };
    //@ts-ignore
    JournalDirectory.prototype._onClickEntityName = onClickEntityName;
  }
});

Hooks.once("ready", function () {
  console.log("One Journal | Initializing One Journal");
  if (!window.oneJournal.userPermitted()) {
    window.oneJournal.openButton?.css("display", "none");
    console.log("One Journal | disabled for user");
    return;
  }
  window.oneJournal.init();
  if (game.modules.get("popout")) {
    popOutHacks();
  }
});

Hooks.on("collapseSidebar", (app, collapsed) => {
  document.body.classList.toggle("sidebar-collapsed", collapsed);
});

// Hacks for popout module
function popOutHacks() {
  // Opening journals from inside the popout should not act as a dialog
  Object.defineProperty(JournalSheet.prototype, "options", {
    get: function () {
      const detaching = window.oneJournal?.shell.detachedJournals.has(
        this.document?.uuid
      );
      this._options.popOutModuleDisable = !detaching;
      return this._options;
    },
    set: function (value) {
      this._options = value;
    },
  });
}
